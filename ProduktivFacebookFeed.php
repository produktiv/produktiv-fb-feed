<?php
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

require_once 'facebook/autoload.php';
require_once 'facebook/Exceptions/FacebookResponseException.php';
require_once 'facebook/Exceptions/FacebookSDKException.php';
require_once 'facebook/Helpers/FacebookRedirectLoginHelper.php';

class ProduktivFacebookFeed {
    public $options;

    function __construct($options) {
        $this->options = $options;
    }

    public function getFbFeed() {
        $fb = new Facebook([
            'app_id' => $this->options['app_id'],
            'app_secret' => $this->options['app_secret'],
            'default_graph_version' => 'v3.1'
        ]);

        $userPosts = $fb->get($this->options['graph_api_endpoint'], $this->options['access_token']);
        return $postBody = $userPosts->getDecodedBody();
    }
}
